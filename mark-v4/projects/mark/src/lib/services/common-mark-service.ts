import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { AlertController, ToastController, Events } from 'ionic-angular';

import * as commonmark from 'commonmark';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateTimeUtil, DateIdUtil } from '@ngcore/core';
import { AppUser } from '@ngcore/base';

import { MarkdownPreprocessingUtil } from '../common/util/markdown-preprocessing-util';
import { CommonMarkUtil } from '../common/util/common-mark-util';


@Injectable({
  providedIn : 'root'
})
export class CommonMarkService {

  constructor(
    // private alertCtrl: AlertController,
    // private toastCtrl: ToastController,
    // private events: Events,
  ) {
    // if(isDL()) dl.log('Hello CommonMarkService Provider');

  }

  // tbd:
  // ...

  // public convertToHTML(
  //   markdownStr: string,
  //   imgUrlPrefix: (string | null),
  //   showDebugLog = false,
  //   options: any = {}
  // ): (string | null) {
  //   if (imgUrlPrefix) {
  //     markdownStr = MarkdownPreprocessingUtil.modifyImageURL(markdownStr, imgUrlPrefix, showDebugLog);
  //   }
  //   let html = CommonMarkUtil.convertToHTML(markdownStr, showDebugLog, options);
  //   return html;
  // }

}
