import { NgModule, ModuleWithProviders, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgCoreCoreModule } from '@ngcore/core';
import { NgCoreBaseModule } from '@ngcore/base';
import { NgCoreIdleModule } from '@ngcore/idle';

import { CommonMarkService } from './services/common-mark-service';
import { CommonTextEntryComponent } from './components/common-text-entry/common-text-entry';
import { CommonHtmlEntryComponent } from './components/common-html-entry/common-html-entry';
import { CommonMarkEntryComponent } from './components/common-mark-entry/common-mark-entry';
// import { CommonMarkEditorComponent } from './components/common-mark-editor/common-mark-editor';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    NgCoreCoreModule.forRoot(),
    NgCoreBaseModule.forRoot(),
    NgCoreIdleModule.forRoot()
  ],
  declarations: [
    CommonTextEntryComponent,
    CommonHtmlEntryComponent,
    CommonMarkEntryComponent,
    // CommonMarkEditorComponent
  ],
  exports: [
    CommonTextEntryComponent,
    CommonHtmlEntryComponent,
    CommonMarkEntryComponent,
    // CommonMarkEditorComponent
  ],
  entryComponents: [
    CommonTextEntryComponent,
    CommonHtmlEntryComponent,
    CommonMarkEntryComponent
  ]
})
export class NgCoreMarkModule {
  static forRoot(): ModuleWithProviders<NgCoreMarkModule> {
    return {
      ngModule: NgCoreMarkModule,
      providers: [
        CommonMarkService
      ]
    };
  }
}
