import * as commonmark from 'commonmark';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
// import { DateIdUtil } from '@ngcore/core';


// Not implemented.
// Do not use these functions here...
export namespace MarkdownPreprocessingUtil {

  // Note:
  // "Preprocessing" does not really make sense.
  // Whatever modification needs to be made
  // should be done during parsing/rendering to HTML.

  // TBD:
  export function externalizeURL(inputMarkdown: string, showDebugLog = false): string {
    if (showDebugLog) {
      if(isDL()) dl.log('MarkdownPreprocessingUtil: Calling externalizeURL() with inputMarkdown = ' + inputMarkdown);
    }
    let outputMarkdown: string = '';

    // temporary
    outputMarkdown = inputMarkdown;
    

    if (showDebugLog) {
      if(isDL()) dl.log('MarkdownPreprocessingUtil: externalizeURL(). outputMarkdown = ' + outputMarkdown);
    }
    return outputMarkdown;
  }

  // TBD:
  export function modifyImageURL(inputMarkdown: string, imgUrlPrefix: string, showDebugLog = false): string {
    if (showDebugLog) {
      if(isDL()) dl.log('MarkdownPreprocessingUtil: Calling modifyImageURL() with inputMarkdown = ' + inputMarkdown + "; imgUrlPrefix = " + imgUrlPrefix);
    }
    let outputMarkdown: string = '';

    // temporary
    outputMarkdown = inputMarkdown;


    if (showDebugLog) {
      if(isDL()) dl.log('MarkdownPreprocessingUtil: modifyImageURL(). outputMarkdown = ' + outputMarkdown);
    }
    return outputMarkdown;
  }

}
