import * as commonmark from 'commonmark';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
// import { DateIdUtil } from '@ngcore/core';

import { MarkdownPreprocessingUtil } from './markdown-preprocessing-util';


/**
 * Utility functions for markdown processing.
 */
export namespace CommonMarkUtil {

  /**
   * Processes the markdown to HTML.
   * 
   * @param markdownStr Input markdown
   * @param showDebugLog If true, debuglog is displayed.
   * @param options CommonMark option.
   */
  export function convertToHTML(markdownStr: string, showDebugLog = false, options: any = {}): (string | null) {
    if (showDebugLog) {
      if(isDL()) dl.log('CommonMarkUtil: convertToHTML() with markdownStr = ' + markdownStr);
    }

    let htmlStr: (string | null) = null;
    try {
      // Set the default value of "safe" to true;
      if (!("safe" in options)) {
        options.safe = true;
      }
      let reader = new commonmark.Parser();
      // let writer = new commonmark.HtmlRenderer({ safe: true });
      let writer = new commonmark.HtmlRenderer(options);
      // writer.softbreak = "<br />";  // This throws excetion "this[type] is not a function"
      let parsed = reader.parse(markdownStr);
      if (showDebugLog) {
        if(isDL()) dl.log('CommonMarkUtil: convertToHTML() successfully parsed: ' + parsed);
      }
      htmlStr = writer.render(parsed);
    } catch (ex) {
      if(isDL()) dl.log('CommonMarkUtil: convertToHTML() Failed: error = ' + ex);
    }

    if (showDebugLog) {
      if(isDL()) dl.log('CommonMarkUtil: convertToHTML() generated htmlStr = ' + htmlStr);
    }
    return htmlStr;
  }

  /**
   * Processes the markdown to HTML.
   * 
   * @param markdownStr Input markdown
   * @param imgUrlPrefix If specified, image urls will be modified with the given prefix.
   * @param showDebugLog If true, debuglog is displayed.
   * @param options CommonMark option.
   */
  export function transformToHTML(
    markdownStr: string,
    imgUrlPrefix: (string | null) = null,
    showDebugLog = false,
    options: any = {}
  ): (string | null) {
    if (showDebugLog) {
      if(isDL()) dl.log('CommonMarkUtil: transformToHTML() with markdownStr = ' + markdownStr);
    }

    let htmlStr: (string | null) = null;
    try {
      // Set the default value of "safe" to true;
      if (!("safe" in options)) {
        options.safe = true;
      }
      let reader = new commonmark.Parser();
      // let writer = new commonmark.HtmlRenderer({ safe: true });
      let writer = new commonmark.HtmlRenderer(options);
      // writer.softbreak = "<br />";  // This throws excetion "this[type] is not a function"
      let parsed = reader.parse(markdownStr);
      if (showDebugLog) {
        if(isDL()) dl.log('CommonMarkUtil: transformToHTML() successfully parsed: ' + parsed);
      }

      if (imgUrlPrefix) {
        if (!imgUrlPrefix.endsWith('/')) {
          imgUrlPrefix += '/';
        }
        let parentPrefix = null;
        let x = imgUrlPrefix.substring(0, imgUrlPrefix.length - 2).lastIndexOf('/');
        if (x != -1) {
          parentPrefix = imgUrlPrefix.substring(0, x + 1);
        }
        let walker = parsed.walker();
        let step: commonmark.NodeWalkingStep;
        while ((step = walker.next())) {
          let node = step.node;
          if (step.entering) {
            if (!!imgUrlPrefix && node.type === 'image') {
              let destination = node.destination;
              let title = node.title;
              let literal = node.literal;
              if (showDebugLog) {
                if(isDL()) dl.log(`>>> Image found: destination = ${destination}; title = ${title}; literal = ${literal}`);
              }
              let c = node.firstChild;
              if (c) {
                if (showDebugLog) {
                  if(isDL()) dl.log(`Child = ${c.literal}`);
                }
              }
              if (destination) {
                // tbd:
                // Exclude absolute urls.
                if (destination.startsWith('http') || destination.startsWith('/')) {
                  // ignore
                } else {
                  let modifiedDestination: string;
                  if (destination.startsWith('../')) {  // Supports one-level up parent folder.
                    if (parentPrefix) {
                      modifiedDestination = parentPrefix + destination.substring(3);
                    } else {
                      // ???
                      modifiedDestination = destination.substring(2);   // ???
                    }
                  } else {
                    modifiedDestination = imgUrlPrefix + destination;
                  }
                  if (showDebugLog) {
                    if(isDL()) dl.log(`modifiedDestination = ${modifiedDestination}`);
                  }
                  // tbd:
                  node.destination = modifiedDestination;
                }
              }
            }
          }
        }
      }

      htmlStr = writer.render(parsed);
    } catch (ex) {
      if(isDL()) dl.log('CommonMarkUtil: transformToHTML() Failed: error = ' + ex);
    }

    if (showDebugLog) {
      if(isDL()) dl.log('CommonMarkUtil: transformToHTML() generated htmlStr = ' + htmlStr);
    }
    return htmlStr;
  }


  /**
   * Processes the markdown to HTML.
   * 
   * @param markdownStr Input markdown
   * @param imgUrlPrefix If specified, image urls will be modified with the given prefix.
   * @param externalizeLink If true, the link will be displayed as text.
   * @param showDebugLog If true, debuglog is displayed.
   * @param options CommonMark option.
   */
  export function transliterateToHTML(
    markdownStr: string,
    imgUrlPrefix: (string | null) = null,
    externalizeLink: boolean = false,
    // etc...
    showDebugLog = false,
    options: any = {}
  ): (string | null) {
    // if (imgUrlPrefix) {
    //   markdownStr = MarkdownPreprocessingUtil.modifyImageURL(markdownStr, imgUrlPrefix, showDebugLog);
    // }
    // let html = CommonMarkUtil.convertToHTML(markdownStr, showDebugLog, options);
    // return html;

    if (showDebugLog) {
      if(isDL()) dl.log('CommonMarkUtil: transliterateToHTML() with markdownStr = ' + markdownStr);
    }

    let htmlStr: (string | null) = null;
    try {
      // Set the default value of "safe" to true;
      if (!("safe" in options)) {
        options.safe = true;
      }
      let reader = new commonmark.Parser();
      // let writer = new commonmark.HtmlRenderer({ safe: true });
      let writer = new commonmark.HtmlRenderer(options);
      // writer.softbreak = "<br />";  // This throws excetion "this[type] is not a function"
      let parsed = reader.parse(markdownStr);
      if (showDebugLog) {
        if(isDL()) dl.log('CommonMarkUtil: transliterateToHTML() successfully parsed: ' + parsed);
      }

      if (!!imgUrlPrefix || externalizeLink) {
        let parentPrefix = null;
        if (imgUrlPrefix) {
          if (!imgUrlPrefix.endsWith('/')) {
            imgUrlPrefix += '/';
          }
          let x = imgUrlPrefix.substring(0, imgUrlPrefix.length - 2).lastIndexOf('/');
          if (x != -1) {
            parentPrefix = imgUrlPrefix.substring(0, x + 1);
          }
        }
        let walker = parsed.walker();
        let step: commonmark.NodeWalkingStep;
        while ((step = walker.next())) {
          let node = step.node;
          if (step.entering) {
            if (!!imgUrlPrefix && node.type === 'image') {
              let destination = node.destination;
              let title = node.title;
              let literal = node.literal;
              if (showDebugLog) {
                if(isDL()) dl.log(`>>> Image found: destination = ${destination}; title = ${title}; literal = ${literal}`);
              }
              let c = node.firstChild;
              if (c) {
                if (showDebugLog) {
                  if(isDL()) dl.log(`Child = ${c.literal}`);
                }
              }
              if (destination) {
                // tbd:
                // Exclude absolute urls.
                if (destination.startsWith('http') || destination.startsWith('/')) {
                  // ignore
                } else {
                  let modifiedDestination: string;
                  if (destination.startsWith('../')) {  // Supports one-level up parent folder.
                    if (parentPrefix) {
                      modifiedDestination = parentPrefix + destination.substring(3);
                    } else {
                      // ???
                      modifiedDestination = destination.substring(2);   // ???
                    }
                  } else {
                    modifiedDestination = imgUrlPrefix + destination;
                  }
                  if (showDebugLog) {
                    if(isDL()) dl.log(`modifiedDestination = ${modifiedDestination}`);
                  }
                  // tbd:
                  node.destination = modifiedDestination;
                }
              }
            }
            if (externalizeLink && node.type === 'link') {
              let destination = node.destination;
              let title = node.title;
              let literal = node.literal;
              if (showDebugLog) {
                if(isDL()) dl.log(`>>> Link found: destination = ${destination}; title = ${title}; literal = ${literal}`);
              }
              if (destination) {
                let text = '';
                let c = node.firstChild;
                if (c) {
                  if (showDebugLog) {
                    if(isDL()) dl.log(`Child = ${c.literal}`);
                  }
                  text = c.literal;
                }
                // tbd:
                let modifiedText = text + ' (' + destination + ')';
                if (showDebugLog) {
                  if(isDL()) dl.log(`modifiedText = ${modifiedText}`);
                }
                // tbd:
                c.literal = modifiedText;
              }
            }
          }
        }
      }

      htmlStr = writer.render(parsed);
    } catch (ex) {
      if(isDL()) dl.log('CommonMarkUtil: transliterateToHTML() Failed: error = ' + ex);
    }

    if (showDebugLog) {
      if(isDL()) dl.log('CommonMarkUtil: transliterateToHTML() generated htmlStr = ' + htmlStr);
    }
    return htmlStr;
  }


  /**
   * Processes the markdown to HTML.
   * 
   * @param markdownStr Input markdown
   * @param imgUrlPrefix If specified, image urls will be modified with the given prefix.
   * @param linkUrlPrefix If specified, links starting with pageLinkMarker will be prepended with the given prefix.
   * @param pageLinkMarker Indicator for link url modification.
   * @param removeMarker If true, the marker is removed when the prefix is prepended.
   * @param showDebugLog If true, debuglog is displayed.
   * @param options CommonMark option.
   */
  export function transmuteToHTML(
    markdownStr: string,
    imgUrlPrefix: (string | null) = null,
    linkUrlPrefix: (string | null) = null,
    pageLinkMarker: string = '#',
    removeMarker: boolean = false,  // tbd: option to replace marker with a different string?
    // etc...
    showDebugLog = false,
    options: any = {}
  ): (string | null) {
    if (showDebugLog) {
      if(isDL()) dl.log('CommonMarkUtil: transmuteToHTML() with markdownStr = ' + markdownStr);
    }

    let htmlStr: (string | null) = null;
    try {
      // Set the default value of "safe" to true;
      if (!("safe" in options)) {
        options.safe = true;
      }
      let reader = new commonmark.Parser();
      // let writer = new commonmark.HtmlRenderer({ safe: true });
      let writer = new commonmark.HtmlRenderer(options);
      // writer.softbreak = "<br />";  // This throws excetion "this[type] is not a function"
      let parsed = reader.parse(markdownStr);
      if (showDebugLog) {
        if(isDL()) dl.log('CommonMarkUtil: transmuteToHTML() successfully parsed: ' + parsed);
      }

      if (!!imgUrlPrefix || linkUrlPrefix) {
        let parentPrefix = null;
        if (imgUrlPrefix) {
          if (!imgUrlPrefix.endsWith('/')) {
            imgUrlPrefix += '/';
          }
          let x = imgUrlPrefix.substring(0, imgUrlPrefix.length - 2).lastIndexOf('/');
          if (x != -1) {
            parentPrefix = imgUrlPrefix.substring(0, x + 1);
          }
        }
        let markerLength = 0;
        if(linkUrlPrefix) {
          if(!pageLinkMarker) {
            pageLinkMarker = '#';   // Use hash anchor tag by default.
          }
          markerLength = pageLinkMarker.length;
        }
        let walker = parsed.walker();
        let step: commonmark.NodeWalkingStep;
        while ((step = walker.next())) {
          let node = step.node;
          if (step.entering) {
            if (!!imgUrlPrefix && node.type === 'image') {
              let destination = node.destination;
              let title = node.title;
              let literal = node.literal;
              if (showDebugLog) {
                if(isDL()) dl.log(`>>> Image found: destination = ${destination}; title = ${title}; literal = ${literal}`);
              }
              let c = node.firstChild;
              if (c) {
                if (showDebugLog) {
                  if(isDL()) dl.log(`Child = ${c.literal}`);
                }
              }
              if (destination) {
                // tbd:
                // Exclude absolute urls.
                if (destination.startsWith('http') || destination.startsWith('/')) {
                  // ignore
                } else {
                  let modifiedDestination: string;
                  if (destination.startsWith('../')) {  // Supports one-level up parent folder.
                    if (parentPrefix) {
                      modifiedDestination = parentPrefix + destination.substring(3);
                    } else {
                      // ???
                      modifiedDestination = destination.substring(2);   // ???
                    }
                  } else {
                    modifiedDestination = imgUrlPrefix + destination;
                  }
                  if (showDebugLog) {
                    if(isDL()) dl.log(`modifiedDestination = ${modifiedDestination}`);
                  }
                  // tbd:
                  node.destination = modifiedDestination;
                }
              }
            }
            if (linkUrlPrefix && node.type === 'link') {
              let destination = node.destination;
              let title = node.title;
              let literal = node.literal;
              if (showDebugLog) {
                if(isDL()) dl.log(`>>> Link found: destination = ${destination}; title = ${title}; literal = ${literal}`);
              }
              if (destination) {
                if(destination.startsWith(pageLinkMarker)) {
                  if(removeMarker) {
                    destination = destination.substring(markerLength);
                  }
                  let modifiedPageLink = linkUrlPrefix + destination;
                  node.destination = modifiedPageLink;
                }
              }
            }
          }
        }
      }

      htmlStr = writer.render(parsed);
    } catch (ex) {
      if(isDL()) dl.log('CommonMarkUtil: transmuteToHTML() Failed: error = ' + ex);
    }

    if (showDebugLog) {
      if(isDL()) dl.log('CommonMarkUtil: transmuteToHTML() generated htmlStr = ' + htmlStr);
    }
    return htmlStr;
  }



}
