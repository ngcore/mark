import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
// import * as commonmark from 'commonmark';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateIdUtil } from '@ngcore/core';
import { LazyLoaderService } from '@ngcore/idle';
import { CommonMarkUtil } from '../../common/util/common-mark-util';


/**
 * Component for displaying markdown (as HTML).
 */
@Component({
  selector: 'common-mark-entry',
  template: `
  <span *ngIf="isInline" [innerHTML]="htmlText"></span>
  <div *ngIf="!isInline" [innerHTML]="htmlText"></div>
`
})
export class CommonMarkEntryComponent {

  // @Input("iid") itemId: string;
  @Input("inline") isInline: boolean = false;
  @Input("url") markdownUrl: string;
  @Input("delay") delay: (number | number[]) = 0;
  @Input("lazy-loaded") lazyLoaded: boolean = false;
  @Input("ignore-cache") ignoreCache: boolean = false;
  @Input("markdown") markdownInput: string;
  @Input("max-retries") maxRetries: number = 0;
  @Input("retry-interval") retryInterval: (number | number[]) = 0;
  @Input("debug-enabled") showDebugLog: boolean = false;
  @Input("renderer-options") rendererOptions: any = {};
  @Input("img-prefix") imgUrlPrefix: (string | null) = null;
  @Input("link-prefix") linkUrlPrefix: (string | null) = null;
  @Input("link-marker") pageLinkMarker: string = '#';   // Use hash tag by default.
  @Input("remove-marker") removeMarker: boolean = false;

  htmlText: string;

  constructor(
    private lazyLoaderService: LazyLoaderService
  ) {
    // testing
    // this.htmlText = "<em>Hello</em>";
    this.htmlText = "";
  }

  ngOnInit() {
    // if(isDL()) dl.log(">>>>>> this.markdownUrl = " + this.markdownUrl);
    // if(isDL()) dl.log(">>>>>> this.markdownInput = " + this.markdownInput);

    // TBD:
    // markdownInput takes precedence over markdownUrl
    if (this.markdownInput) {
      let converted: (string | null);
      if (this.linkUrlPrefix) {
        converted = CommonMarkUtil.transmuteToHTML(this.markdownInput, this.imgUrlPrefix, this.linkUrlPrefix, this.pageLinkMarker, this.removeMarker, this.showDebugLog, this.rendererOptions);
      } else {
        converted = CommonMarkUtil.transformToHTML(this.markdownInput, this.imgUrlPrefix, this.showDebugLog, this.rendererOptions);
      }
      this.htmlText = (converted) ? converted : '';
    } else {
      if (this.markdownUrl) {
        // if(lazyLoaded == true) ??
        // ...

        // tbd: By default, useCache == true.
        let useCache = !this.ignoreCache;
        // tbd: cast delay to number if it's not already so???
        this.lazyLoaderService.loadTextDelayed(this.markdownUrl, this.delay, useCache, this.maxRetries, this.retryInterval).subscribe((content: string) => {
          if (isDL()) dl.log(`Content read from url = ${this.markdownUrl}`);
          // if(isDL()) dl.log(`>> Content: ${content}`);
          let converted: (string | null);
          if (this.linkUrlPrefix) {
            converted = CommonMarkUtil.transmuteToHTML(content, this.imgUrlPrefix, this.linkUrlPrefix, this.pageLinkMarker, this.removeMarker, this.showDebugLog, this.rendererOptions);
          } else {
            converted = CommonMarkUtil.transformToHTML(content, this.imgUrlPrefix, this.showDebugLog, this.rendererOptions);
          }
          this.htmlText = (converted) ? converted : '';
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              if (isDL()) dl.log('An error occurred:', err.error.message);
            } else {
              if (isDL()) dl.log(`Status code: ${err.status}; Error: ${err.error}`);
            }
          }
        );
      }
    }
    // ....
  }

  setMarkdownInput(_markdownInput: string, _imgUrlPrefix: (string | null) = null, _showDebugLog: boolean = false, _rendererOptions: any = {}) {
    this.markdownInput = _markdownInput;
    // if(!_imgUrlPrefix) {   // ???
    //   _imgUrlPrefix = this.imgUrlPrefix;
    // }
    let oldHTML = this.htmlText;   // Last successful conversion....
    let converted = CommonMarkUtil.transformToHTML(this.markdownInput, _imgUrlPrefix, _showDebugLog, _rendererOptions);
    if (converted == null) {
      if (isDL()) dl.log('CommonMarkEntryComponent: setMarkdownInput() Failed conversion: _markdownInput = ' + _markdownInput);
    }
    // this.htmlText = (converted) ? converted : '';
    this.htmlText = (converted != null) ? converted : oldHTML;   // ????
  }

  setMarkdownContent(
    _markdownInput: string,
    _imgUrlPrefix: (string | null) = null,
    _linkUrlPrefix: (string | null) = null,
    _pageLinkMarker: string = '#',
    _removeMarker: boolean = false,
    _showDebugLog: boolean = false,
    _rendererOptions: any = {}
  ) {
    this.markdownInput = _markdownInput;
    // if(!_imgUrlPrefix) {   // ???
    //   _imgUrlPrefix = this.imgUrlPrefix;
    // }
    // ...
    let oldHTML = this.htmlText;   // Last successful conversion....
    let converted = CommonMarkUtil.transmuteToHTML(this.markdownInput, _imgUrlPrefix, _linkUrlPrefix, _pageLinkMarker, _removeMarker, _showDebugLog, _rendererOptions);
    if (converted == null) {
      if (isDL()) dl.log('CommonMarkEntryComponent: setMarkdownContent() Failed conversion: _markdownInput = ' + _markdownInput);
    }
    // this.htmlText = (converted) ? converted : '';
    this.htmlText = (converted != null) ? converted : oldHTML;   // ????
  }

}
