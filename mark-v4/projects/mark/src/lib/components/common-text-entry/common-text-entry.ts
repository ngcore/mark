import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateIdUtil } from '@ngcore/core';
import { LazyLoaderService } from '@ngcore/idle';
// import { MarkEvents } from '../../common/events/mark-events';


/**
 * Component for displaying text content in HTML.
 */
@Component({
  selector: 'common-text-entry',
  template: `
  <span *ngIf="isInline" [innerHTML]="plainText"></span>
  <div *ngIf="!isInline" [innerHTML]="plainText"></div>
`
// TBD:
// --> Use something like this for "common-text-editor" component
// <div >
// <textarea [placeholder]="placeholderText" [(ngModel)]="plainText" (ngModelChange)="textChanged($event)"  #textArea style="border:1px solid silver; padding:2px; min-height:200px; width:100%;"></textarea>
// </div>
// ...
})
export class CommonTextEntryComponent {

  // @Input("iid") itemId: string;
  @Input("inline") isInline: boolean = false;
  @Input("url") textUrl: string;
  @Input("delay") delay: (number | number[]) = 0;
  @Input("lazy-loaded") lazyLoaded: boolean = false;
  @Input("ignore-cache") ignoreCache: boolean = false;
  @Input("text") textInput: string;
  @Input("placeholder") defaultPlaceholder: string;
  @Input("max-retries") maxRetries: number = 0;
  @Input("retry-interval") retryInterval: (number | number[]) = 0;

  plainText: string;
  placeholderText: string;
  isDirty: boolean = false;

  constructor(
    private lazyLoaderService: LazyLoaderService
  ) {
    // if(isDL()) dl.log('Hello CommonTextEntry Component. id = ' + this.navCtrl.id);

    // testing
    // this.plainText = "Hello";
    this.plainText = "";
    this.placeholderText = "";
  }
  ngOnInit() {
    // // variables can be read here....
    // // if(isDL()) dl.log(">>>>>> this.itemId = " + this.itemId);
    // if(isDL()) dl.log(">>>>>> this.textInput = " + this.textInput);

    // TBD:
    if (this.textInput) {
      this.plainText = this.textInput;
    } else {
      if (this.textUrl) {
        // if(lazyLoaded == true) ??
        // ...

        // tbd: By default, useCache == true.
        let useCache = !this.ignoreCache;
        this.lazyLoaderService.loadTextDelayed(this.textUrl, this.delay, useCache, this.maxRetries, this.retryInterval).subscribe((content: string) => {
          if(isDL()) dl.log(`Content read from url = ${this.textUrl}`);
          // if(isDL()) dl.log(`Content: ${content}`);
          this.plainText = content;
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              if(isDL()) dl.log('An error occurred:', err.error.message);
            } else {
              if(isDL()) dl.log(`Status code: ${err.status}; Error: ${err.error}`);
            }
          }
        );
      }
    }
    if (this.defaultPlaceholder) {
      this.placeholderText = this.defaultPlaceholder;
    }
    // ....
  }

  textChanged(newText) {
    // if(isDL()) dl.log(">>>>>> textChanged() newText = " + newText);

    this.plainText = newText;
    this.isDirty = true;
    // tbd
    // this.events.publish(MarkEvents.MVP0_TEXT_ENTRY_TEXT_CHANGED, this.plainText);
  }

  setTextInput(_textInput: string) {
    this.textInput = _textInput;
    this.isDirty = true;
    this.plainText = this.textInput;
  }
  setDefaultPlaceholder(_placeholder: string) {
    this.defaultPlaceholder = _placeholder;
    this.placeholderText = this.defaultPlaceholder;
  }

}
