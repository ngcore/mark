import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateIdUtil } from '@ngcore/core';
import { LazyLoaderService } from '@ngcore/idle';


/**
 * Component for displaying HTML content.
 */
@Component({
  selector: 'common-html-entry',
  template: `
  <span *ngIf="isInline" [innerHTML]="htmlText"></span>
  <div *ngIf="!isInline" [innerHTML]="htmlText"></div>
`
})
export class CommonHtmlEntryComponent {

  // @Input("iid") itemId: string;
  @Input("inline") isInline: boolean = false;
  @Input("url") htmlUrl: string;
  @Input("delay") delay: (number | number[]) = 0;
  @Input("lazy-loaded") lazyLoaded: boolean = false;
  @Input("ignore-cache") ignoreCache: boolean = false;
  @Input("html") htmlInput: string;
  // @Input("placeholder") defaultPlaceholder: string;
  @Input("max-retries") maxRetries: number = 0;
  @Input("retry-interval") retryInterval: (number | number[]) = 0;

  htmlText: string;

  constructor(
    private lazyLoaderService: LazyLoaderService
  ) {
    // if(isDL()) dl.log('Hello CommonHtmlEntry Component. id = ' + this.navCtrl.id);

    this.htmlText = "";
  }
  ngOnInit() {
    // // variables can be read here....
    // // if(isDL()) dl.log(">>>>>> this.itemId = " + this.itemId);
    // if(isDL()) dl.log(">>>>>> this.htmlInput = " + this.htmlInput);

    // TBD:
    if (this.htmlInput) {
      this.htmlText = this.htmlInput;
    } else {
      if (this.htmlUrl) {
        // if(lazyLoaded == true) ??
        // ...

        // tbd: By default, useCache == true.
        let useCache = !this.ignoreCache;
        this.lazyLoaderService.loadTextDelayed(this.htmlUrl, this.delay, useCache, this.maxRetries, this.retryInterval).subscribe((content: string) => {
          if(isDL()) dl.log(`Content read from url = ${this.htmlUrl}`);
          // if(isDL()) dl.log(`Content: ${content}`);
          this.htmlText = content;
        },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              if(isDL()) dl.log('An error occurred:', err.error.message);
            } else {
              if(isDL()) dl.log(`Status code: ${err.status}; Error: ${err.error}`);
            }
          }
        );
      }
    }
    // ....
  }

  setHtmlInput(_htmlInput: string) {
    this.htmlInput = _htmlInput;
    this.htmlText = this.htmlInput;
  }

}
