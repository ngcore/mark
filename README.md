# @ngcore/mark
> NG Core angular/typescript mark library


Markup classes library for Angular.
(Current version requires Angular v7.2+)

We currently use `commonmark.js`.
(But, the idea is that the underlying markdown library should be pluggable.)



## API Docs

* http://ngcore.gitlab.io/mark/



